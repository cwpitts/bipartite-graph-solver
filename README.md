# Bipartite Graph Solver
A simple graphical solver for bipartite graph problems.

## Requirements
* Qt 5.10
* GNU Make 4.2.1
* C++ 14

## Compilation
```bash
mkdir buold
cd build
qmake -o Makefile ../src/bipartite-graphs.pro -spec linux-g++
make
```

## Running the program
```bash
./bipartite-graph-solver
```
