#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QString>
#include <QStringList>
#include <cstdlib>
#include <time.h>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->V = NULL;
    this->E = NULL;
    srand(time(NULL));
}

MainWindow::~MainWindow()
{
    delete ui;

    if (this->V != NULL)
    {
	delete[] V;
    }

    if (this->E != NULL)
    {
	delete[] E;
    }
}

void MainWindow::generate_graph()
{
    // Make sure we start with a clean slate
    if (this->V != NULL)
    {
	delete[] V;
    }

    if (this->E != NULL)
    {
	delete[] E;
    }

    // Use only the space we actually have in the graphicsView
    unsigned width = this->ui->graphicsView->width();
    unsigned height = this->ui->graphicsView->height();

    // Generate vertices
    this->numVertices = this->ui->vertexSpinBox->value();

    this->ui->solutionOutput->append("Creating " + QString::number(this->numVertices) + " vertices...");
    
    this->V = new struct vertex[this->numVertices];
    for (unsigned i = 0; i < this->numVertices; i++)
    {
	this->V[i].x = rand() % width;
	this->V[i].y = rand() % height;
    }

    // Create edges
    this->numEdges = this->ui->edgeSpinBox->value();

    this->ui->solutionOutput->append("Creating " + QString::number(this->numEdges) + " edges...");
    
    this->E = new unsigned*[this->numVertices];
    for (unsigned i = 0; i < this->numVertices; i++)
    {
	this->E[i] = new unsigned[this->numVertices];
	
	for (unsigned j = 0; j < this->numVertices; j++)
	{
	    this->E[i][j] = NOEDGE;
	}
    }

    // Store edge information in E
    for (unsigned i = 0; i < this->numEdges; i++)
    {
	unsigned v1 = rand() % this->numVertices;
	unsigned v2 = rand() % this->numVertices;

	// Avoid self-connecting edges and duplicated edges
	while (v2 == v1 || this->E[v1][v2] == EDGE || this->E[v2][v1] == EDGE)
	{
	    v1 = rand() % this->numVertices;
	    v2 = rand() % this->numVertices;
	}

	// Mark edge in E
	this->E[v1][v2] = EDGE;
	this->E[v2][v1] = EDGE;
    }

    // Draw vertices and edges
    QGraphicsView* view = this->ui->graphicsView;
    QGraphicsScene* scene = new QGraphicsScene();

    view->setScene(scene);

    for (unsigned i = 0; i < this->numVertices; i++)
    {
	scene->addEllipse(this->V[i].x, this->V[i].y, 5, 5);
	for (unsigned j = i; j < this->numVertices; j++)
	{
	    if (this->E[i][j] == EDGE)
	    {
		scene->addLine(this->V[i].x, this->V[i].y,
			       this->V[j].x, this->V[j].y);
	    }
	}
    }    
}

bool MainWindow::solve_graph()
{
    // bool inV0[this->numVertices] { false };
    // bool inV1[this->numVertices] { false };


    struct bipartite solution;
    solution.isBipartite = true;
    solution.inV0 = new bool[this->numVertices] { false };
    solution.inV1 = new bool[this->numVertices] { false };

    for (unsigned i = 0; i < this->numVertices; i++)
    {
	QString debugStr = "";
	
	for (unsigned j = 0; j < this->numVertices; j++)
	{
	    debugStr += QString::number(this->E[i][j]);
	    debugStr += " ";
	}
	qDebug() << debugStr;
    }

    for (unsigned i = 0; i < this->numVertices; i++)
    {
	if (solution.isBipartite == true)
	{
	    this->partition(i, solution);   
	}
    }

    if (solution.isBipartite == true)
    {
	this->update_solution("Final partitions:");

	QStringList finalV0;
	QStringList finalV1;
	
	for (unsigned i = 0; i < this->numVertices; i++)
	{
	    if (solution.inV0[i] == true)
	    {
		finalV0.append(QString::number(i));
	    }
	    else if (solution.inV1[i] == true)
	    {
		finalV1.append(QString::number(i));
	    }	    
	}

	QString V0 = QString("V0: [%1]").arg(finalV0.join(", "));
	QString V1 = QString("V1: [%1]").arg(finalV1.join(", "));
	
	this->update_solution(V0);
	this->update_solution(V1);
    }

    return solution.isBipartite;
}

void MainWindow::partition(int v0, struct bipartite& solution)
{
    if (solution.inV0[v0] == true)
    {
	this->update_solution("Vertex " + QString::number(v0) + " is in V0");
    }
    else if (solution.inV1[v0] == true)
    {
	this->update_solution("Vertex " + QString::number(v0) + " is in V1");
    }
    else
    {
	this->update_solution("Vertex " + QString::number(v0) + " is not in a partition");
    }

    if (solution.inV0[v0] == false && solution.inV1[v0] == false)
    {
	this->update_solution("Placing vertex " + QString::number(v0) + " in partition 0");
	solution.inV0[v0] = true;
    }

    // Iterate over edges
    for (unsigned vi = 0; vi < this->numVertices; vi++)
    {
	if (this->E[v0][vi] == EDGE)
	{
	    this->update_solution("Edge from " + QString::number(v0) + " to " + QString::number(vi));

	    // Are we in the same partition?
	    if ((solution.inV0[v0] == true && solution.inV0[vi] == true) ||
		(solution.inV1[v0] == true && solution.inV1[vi] == true))
	    {
		this->update_solution("Found contradiction, " + QString::number(v0) + " and " + QString::number(vi) + " are in the same partition!");
		solution.isBipartite = false;
		return;
	    }
	    
	    // If v0 is in V0, put vi in V1, and vice versa
	    if (solution.inV0[v0] == true && solution.inV1[vi] == false)
	    {
		this->update_solution("Placing vertex " + QString::number(vi) + " in partition 1");
		
		solution.inV1[vi] = true;
		this->print_partitions(solution.inV0, solution.inV1);
		this->partition(vi, solution);
	    }
	    else if (solution.inV1[v0] == true && solution.inV0[vi] == false)
	    {
		this->update_solution("Placing vertex " + QString::number(vi) + " in partition 0");
		
		solution.inV0[vi] = true;
		this->print_partitions(solution.inV0, solution.inV1);
		this->partition(vi, solution);
	    }
	}
    }

    solution.isBipartite = true;
}

void MainWindow::print_partitions(bool inV0[], bool inV1[])
{
    QString V0 = "";
    QString V1 = "";

    for (unsigned i = 0; i < this->numVertices; i++)
    {
	if (inV0[i] == true)
	{
	    V0 += QString::number(i);
	    V0 += " ";
	}
	else if (inV1[i] == true)
	{
	    V1 += QString::number(i);
	    V1 += " ";	    
	}
    }

    qDebug() << "V0:" << V0;
    qDebug() << "V1:" << V1;
}

void MainWindow::on_generateButton_clicked()
{
    this->ui->solutionOutput->clear();
    this->generate_graph();
}

void MainWindow::on_solveButton_clicked()
{
    this->ui->solutionOutput->append("Solving bipartite graph...");
    if (this->solve_graph() == false)
    {
	this->ui->solutionOutput->append("No bipartite solution was found.");
    }
    else
    {
	this->ui->solutionOutput->append("Bipartite solution found.");
    }
}

void MainWindow::on_vertexSpinBox_valueChanged(int val)
{
    this->ui->edgeSpinBox->setMinimum(val - 1);

    unsigned maxDensity = 0;
    unsigned i = val;
    while (i > 0)
    {
	maxDensity += i - 1;
	i--;
    }
    
    this->ui->edgeSpinBox->setMaximum(maxDensity);
}

void MainWindow::update_solution(QString txt)
{
    this->ui->solutionOutput->append(txt);
}
