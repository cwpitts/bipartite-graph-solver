struct vertex
{
    unsigned int x;
    unsigned int y;
};

struct bipartite
{
    bool isBipartite;
    bool* inV0;
    bool* inV1;
};

const unsigned EDGE = 1;
const unsigned NOEDGE = 0;
