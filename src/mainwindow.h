#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "graph.h"

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    void generate_graph();
    bool solve_graph();
    void partition(int v0, struct bipartite& solution);
    void print_partitions(bool inV0[], bool inV1[]);
    void update_solution(QString txt);
    
    vertex* V;
    unsigned** E;
    unsigned numVertices;
    unsigned numEdges;

private slots:
    void on_generateButton_clicked();
    void on_vertexSpinBox_valueChanged(int val);
    void on_solveButton_clicked();
};

#endif // MAINWINDOW_H
